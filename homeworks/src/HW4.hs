-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"
instance Show' A where
    show' (A i) = "A " ++ (convert i)
    show' B = "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"
instance Show' C where
    show' (C a b) = "C " ++ (convert a) ++ " " ++ (convert b)

convert num
  | num < 0 = "(" ++ (show num) ++ ")"
  |otherwise = show num

----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set f1) (Set f2) = Set (\elem -> let f1_elem = f1 elem
                                                          f2_elem = f2 elem
                            in f1_elem && not f2_elem || not f1_elem && f2_elem)

-----------------

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool b
    | b = \t -> (\f -> t)
    | otherwise = \t f -> f

-- fromInt - переводит число в кодировку Чёрча
fromInt n = if n == 0 then \s z -> z else \s z -> s $ fromInt (n - 1) s z
