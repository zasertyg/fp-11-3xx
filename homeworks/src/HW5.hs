-- 2016-04-05 / 2016-04-12

module HW5
       ( Parser (..)
       , dyckLanguage
       , Arith (..)
       , arith
       , Optional (..)
       ) where

{--==========  PARSER ==========-}

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) -> Right (c, cs)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (x,r1) -> case parse (many p) r1 of
    Left _ -> Right ([x],r1)
    Right (xs,r2) -> Right (x:xs, r2)

matching :: (Char -> Bool) -> Parser Char
matching p = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) | p c -> Right (c, cs)
         | otherwise -> Left $ "Char "++[c] ++
                        " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit
           -- read <$> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

{--========  END PARSER ========-}

-- ((())())
dyckLanguage :: Parser String
dyckLanguage = Parser $ \s -> if (==0) $ sum $ map (\oper -> if oper == '(' then 1 else -1) s
            then Right (s, "")
            else Left ""

data Arith = Plus Arith Arith
           | Minus Arith Arith
           | Mul Arith Arith
           | Number Int
           | AnyOper Char
           deriving (Eq,Show)

isOperation :: Char -> Bool
isOperation symbol = elem symbol "+-*/"

priority :: Char -> Integer
priority operation = head $ map (\(_,prior) -> prior) $ filter (\(oper, _) -> oper == operation)
            [('(', 0),(')', 1),('+', 2),('-',2),('*', 3),('/',3)]

data Token = Operation Char | Num Int

parseToRPN :: String -> String -> [Token] -> [Token]
parseToRPN "" [] output = output
parseToRPN "" (s:otherStack) output = parseToRPN "" otherStack (output++[Operation s])
parseToRPN str@(symbol:symbols) stack output
    | null stack && isOperation symbol = parseToRPN symbols (symbol : stack) output
    | symbol == '(' = parseToRPN symbols (symbol : stack) output
    | isOperation symbol = stackOpersPriorityHigher symbols stack symbol output
    | symbol == ')' = getTillBrace symbols stack output
    | otherwise = checkEither (parse number str)
        where checkEither (Right (num, leftStr)) = parseToRPN leftStr stack (output++[Num num])
              getTillBrace left stack output =
                case stack of
                    [] -> parseToRPN left stack output
                    oper : otherOpers
                        | oper == '(' -> parseToRPN left otherOpers output
                        | otherwise -> getTillBrace left otherOpers (output++[Operation oper])
              stackOpersPriorityHigher leftStr stack symbol output =
                case stack of
                    [] -> parseToRPN leftStr (symbol:stack) output
                    oper : otherOpers
                        | priority oper > priority symbol -> stackOpersPriorityHigher leftStr otherOpers symbol
                                        (output++[Operation oper])
                        | otherwise -> parseToRPN leftStr (symbol:oper:otherOpers) output

parse2 :: [Arith] -> Int -> Either Err Arith
parse2 [Number n1] _ = Right (Number n1)
parse2 (a1:a2:(AnyOper o):[]) _ = Right $ head $ map (\(_,arith) -> arith) $ filter (\(oper, _) -> oper == o)
                [('+',Plus a1 a2), ('-',Minus a1 a2), ('*',Mul a1 a2)]
parse2 tokens i = case tokens!!i of
    AnyOper o
        | i < 2 || length tokens < 3 -> Left "" --Неправильное выражение
        | o == '+' -> parse2 ((getBefore tokens (i - 2))++[Plus a1 a2]++(getAfter tokens i)) 0
        | o == '-' -> parse2 ((getBefore tokens (i - 2))++[Minus a1 a2]++(getAfter tokens i)) 0
        | o == '*' -> parse2 ((getBefore tokens (i - 2))++[Mul a1 a2]++(getAfter tokens i)) 0
                where (a1, a2) = (tokens!!(i - 2), tokens!!(i - 1)) --Предыдущие два токена; должны быть числами
                      getBefore tokens idx = take idx tokens --Берем первые idx токенов
                      getAfter tokens idx = drop (idx + 1) tokens --Берем последние токены, отбрасывая первые idx
    _ -> parse2 tokens (i + 1)
parse2 _ _ = Left ""

toArith :: [Token] -> [Arith]
toArith = map convertToken
    where convertToken (Operation o) = AnyOper o
          convertToken (Num n) = Number n

arith :: Parser Arith
arith = Parser $ \s -> case parse2 (toArith (parseToRPN s [] [])) 0 of
    Left _ -> Left "WRONG ARITH"
    Right arith -> Right (arith, "")

-- Инстансы функтора и аппликативного функтора

data Optional a = NoParam
                | Param a
                deriving (Eq,Show)

instance Functor Optional where
  fmap f NoParam = NoParam
  fmap f (Param val) = Param (f val)

instance Applicative Optional where
  pure = Param
  (Param f) <*> (Param val) = Param (f val)
  NoParam <*> _ = NoParam
  _ <*> NoParam = NoParam
