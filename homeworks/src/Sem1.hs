module Sem1 where

import Data.Char

data LamVar = LamVar String
    deriving (Eq)
data Term2 = Var String 
        | Lam LamVar Term2
        | App Term2 Term2
        deriving (Eq)

instance Show LamVar where
    show (LamVar var) = var  
instance Show Term2 where  
    show (Lam v t) = "(\\" ++ show v ++ " . " ++ show t ++ ")"
    show (App t1 t2) = show t1 ++ " " ++ show t2
    show (Var v) = v

--Вычисление терма на один шаг
eval1 :: Term2 -> Term2

eval1 (App a1@(App t1 t2) t3) = App (eval1 a1) t3

eval1 (App t1 a1@(App t2 t3)) = App t1 (eval1 a1)

eval1 (App l1@(Lam (LamVar v1) t1) l2) = replaceVar v1 l2 t1 (getUsedVars t1)

eval1 term = term

--Вычисление терма на много шагов
eval_many :: Term2 -> Term2


eval_many (App a1@(App (Var v) t2) t3) = App a1 (eval_many t3)


eval_many (App a1@(App t1 t2) t3) = eval_many $ App (eval_many a1) t3


eval_many (App t1 a1@(App (Var v) t2)) = App (eval_many t1) a1


eval_many (App t1 a1@(App t2 t3)) = eval_many $ App t1 (eval_many a1)

eval_many (App l1@(Lam (LamVar v1) t1) l2) = eval_many $ replaceVar v1 l2 t1 (getUsedVars t1)

eval_many term = term

--Заменить все свободные вхождения переменной var на терм term в терме inTerm2

replaceVar :: String -> Term2 -> Term2 -> [String] -> Term2
replaceVar var term inTerm2 boundVars = case inTerm2 of
    lam@(Lam (LamVar v) (Lam v2 t)) -> if v == var  
                    then lam
                    else Lam (LamVar v) (replaceVar var new_term (Lam v2 t) boundVars)
    lam@(Lam (LamVar v) (App t1 t2)) -> if v == var
                    then lam
                    else Lam (LamVar v) (replaceVar var new_term (App t1 t2) boundVars)
    lam@(Lam (LamVar v) (Var v1)) -> if v /= v1 && v1 == var
                    then Lam (LamVar v) new_term 
                    else lam
    app@(App t1 t2) -> App (replaceVar var new_term t1 boundVars) (replaceVar var new_term t2 boundVars)
    Var v -> if v == var then term else Var v
    where new_term = case term of  
                Var v -> if v `elem` boundVars then Var (fetchUnboundVar boundVars "a") else Var v
                _ -> replaceUnboundVars (getUsedVars term ++ boundVars) boundVars term



fetchUnboundVar :: [String] -> String -> String
fetchUnboundVar boundVars var
    | var `elem` boundVars = let nextVar [ch] = [chr (ord ch + 1)] in fetchUnboundVar boundVars (nextVar var)
    | otherwise = var


replaceUnboundVars :: [String] -> [String] -> Term2 -> Term2
replaceUnboundVars _ [] term = term
replaceUnboundVars boundVarsList (x:xs) term = replaceUnboundVars boundVarsList xs (replaceUnboundVar x term boundVarsList)


replaceUnboundVar :: String -> Term2 -> [String] -> Term2
replaceUnboundVar usedVar term boundVars = case term of
        Var v -> if usedVar == v then Var (fetchUnboundVar boundVars "a") else Var v  
        l@(Lam (LamVar v) t) -> if usedVar == v
                then let onVar = fetchUnboundVar boundVars "a" in Lam (LamVar onVar) (forceReplaceVar v onVar t)
                else Lam (LamVar v) (replaceUnboundVar usedVar t boundVars)
        App t1 t2 -> App (replaceUnboundVar usedVar t1 boundVars) (replaceUnboundVar usedVar t2 boundVars)


forceReplaceVar :: String -> String -> Term2 -> Term2
forceReplaceVar var onVar term = case term of
    Var v -> if v == var then Var onVar else Var v
    Lam (LamVar v) t -> Lam (LamVar v) (forceReplaceVar var onVar t)
    App t1 t2 -> App (forceReplaceVar var onVar t1) (forceReplaceVar var onVar t2)

--Получить все связанные переменные из терма
getUsedVars :: Term2 -> [String]
getUsedVars term = case term of
    Lam (LamVar v) t -> v : getUsedVars t
    App t1 t2 -> getUsedVars t1 ++ getUsedVars t2
    (Var v) -> []