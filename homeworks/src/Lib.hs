-- Do not change! It will be overwritten
-- Use HW*.hs to solve homeworks

module Lib
       ( module Sem1
       , module HW0
       , module HW1
       , module HW2
       , module HW3
       , module HW4
       , module HW5
       ) where

import Sem1
import HW0
import HW1
import HW2
import HW3
import HW4
import HW5

