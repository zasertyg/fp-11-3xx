
-- Новый тип Object
-- может быть либо Chair либо Table
data Object = Chair | Table

-- Сопоставление с образцом
toString :: Object -> String
toString Chair = "Chair"
toString Table = "Table"

-- Нельзя сделать
-- foo Table
foo :: Bool -> String
foo _ = "foo"

-- Кот: жив, мёртв, ни жив ни мёртв
data Cat = Alive
         | Dead
         | Schroedinger

-- Это кот Шрёдингера?
isSchroedinger :: Cat -> Bool
isSchroedinger Schroedinger = True
isSchroedinger _ = False

-- Конструкция case: сопоставление с образцом где угодно
isAliveOrDead :: Cat -> Bool
isAliveOrDead x = case x of
  Alive -> True
  Dead -> True
  _ -> False

-- Ноутбук: ноутбук с толщиной или макбук
-- Конструкторы могут называться так же, как типы
data Notebook = Notebook Double
              | Macbook

-- Notebook 20.3
-- Notebook :: Double -> Notebook
-- Macbook :: Notebook

goodForHipster :: Notebook -> String
goodForHipster Macbook = "Good!"
goodForHipster (Notebook 0) = "Cannot exist!"
goodForHipster (Notebook x)
  | x < 0  = "Better than Macbook"
  | x < 20 = "Good"
  | otherwise = ":/"

thickness :: Notebook -> Double
thickness (Notebook x) = x
thickness _ = undefined

-- Синтаксис записей
data Computer = Server { cores :: Int, memory :: Int }
              | Desktop { display :: Double
                        , memory :: Int }
              deriving (Eq,Show) -- автомагическое
                       -- получение (==) и show

comp1, comp2 :: Computer
comp1 = Server { cores = 8, memory = 32 } -- Server 8 32
comp2 = Desktop { display = 23.1, memory = 8 } -- Desktop 23.1 8
-- можно в любом порядке
comp3 = Desktop { memory = 8, display = 23.1 }
-- нельзя: Desktop 8 23.1
-- нельзя: Server { display = 23.1, memory = 8 }
-- скомпилируется с Warning:
--    Fields of ‘Desktop’ not initialised: display
--    In the expression: Desktop {memory = 8}
comp4 = Desktop { memory = 8 }
-- можно создавать новые значения с измененными полями:
comp5 = comp4 { display = 12 }
comp6 = comp3 { memory = 4, display = 11.2 }
comp7 = comp3 { memory = 4 }

-- Рекурсивные типы данных
data Tree = Leaf Int       -- лист хранит целое число
            -- промежуточный узел с двумя поддеревьями
          | Node Tree Tree      
          deriving (Eq,Show) -- (==), (/=) и show

-- tree1 :: Tree
tree1 = Leaf 1
tree2 = Node tree1 tree1
tree3 = Node tree1 tree2

height :: Tree -> Int
height (Leaf _) = 1
height (Node t1 t2) = 1 + max (height t1) (height t2)

sumTree :: Tree -> Int
sumTree (Leaf x) = x
sumTree (Node t1 t2) = sumTree t1 + sumTree t2

countTree :: Tree -> Int
countTree (Leaf _) = 1
countTree (Node t1 t2) = 1
                       + countTree t1
                       + countTree t2

fibTree0 :: Int -> Tree
fibTree0 0 = Leaf 0
fibTree0 1 = Leaf 0
fibTree0 n = Node (fibTree0 $ n-2) (fibTree0 $ n-1)


